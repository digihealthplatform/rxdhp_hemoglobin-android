package com.dhp.hemoglobin;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class BaseActivityTest extends BaseActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        confirmBackPressed();
    }

    private void confirmBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setMessage(getString(R.string.message_confirm_on_back));
        builder.setPositiveButton(getString(R.string.btn_go_back), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BaseActivityTest.super.onBackPressed();
            }
        });

        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }
}