package com.dhp.hemoglobin;

import android.app.Application;
import android.content.Context;

public class LApplication extends Application {

    private static LApplication instance;

    private static String result;

    public static LApplication get(Context context) {
        return (LApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static Context getAppContext() {
        return instance;
    }

    public static void saveResult(String result) {
        LApplication.result = result;
    }

    public static String getResult() {
        return result;
    }
}