package com.dhp.hemoglobin.opencv;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static org.opencv.imgproc.Imgproc.boundingRect;

public class ImageProcessing {


    public static float findHb(String capture1FileName, String capture2FileName, String capture3FileName, String calibrationFileName) throws Exception {
        Double x, y;

        Mat captured1   = Imgcodecs.imread(capture1FileName);
        Mat captured2   = Imgcodecs.imread(capture2FileName);
        Mat captured3   = Imgcodecs.imread(capture3FileName);
        Mat calibration = Imgcodecs.imread(calibrationFileName);

        float hb = 0;

        Double[] Lvalues = getLValues(calibration);
        Arrays.sort(Lvalues, Collections.<Double>reverseOrder());

        Double Lblood1     = hemoglobin(captured1);
        Double Lblood2     = hemoglobin(captured2);
        Double Lblood3     = hemoglobin(captured3);
        Double Lblood      = (Lblood1 + Lblood2 + Lblood3) / 3.0;
        double affirnity[] = {1, 0.8, 0.6, 0.4, 0.2, 0.1};
        int    values[]    = {4, 6, 8, 10, 12, 14};
        if (Lblood > Lvalues[0]) {
            hb = 4;
        } else if
                (Lblood < Lvalues[5]) {
            hb = 14;
        } else {
            for (int i = 0; i < 6; ++i) {
                if ((Lvalues[i] > Lblood) && (Lblood > Lvalues[i + 1])) {
                    x = (abs(Lblood - Lvalues[i]) * affirnity[i]);
                    y = (abs(Lblood - Lvalues[i + 1]) * affirnity[i + 1]);
                    if (x < y) {
                        hb = values[i];
                    } else {
                        hb = values[i + 1];
                    }
                }

            }
        }
        return hb;
    }


    private static Double hemoglobin(Mat bgr_image) throws Exception {
        Size   size = bgr_image.size();
        double M    = size.height;
        double N    = size.width;

        Mat rgb_image = new Mat(size, CvType.CV_8UC1);
        Imgproc.cvtColor(bgr_image, rgb_image, Imgproc.COLOR_BGR2RGB);

        //Mat crop_img = bgr_image.submat(75, (int) (M), 50, (int) (N - 450));

        Mat smooth    = new Mat(size, CvType.CV_8UC1);
        Mat lab_image = new Mat(size, CvType.CV_8UC1);

        //Imgproc.blur(bgr_image, smooth, new Size(5, 5));
        Imgproc.cvtColor(bgr_image, lab_image, Imgproc.COLOR_BGR2Lab);
        double Lblood = getLblood(bgr_image, lab_image);
        return Lblood;

    }

    private static Double getLblood(Mat crop_img, Mat lab_image) throws Exception {
        Size size        = crop_img.size();
        Mat  median_blur = new Mat(size, CvType.CV_8UC1);
        Mat  gray        = new Mat(size, CvType.CV_8UC1);
        Mat  circles     = new Mat(size, CvType.CV_8UC1);
        Mat  mask        = new Mat(size, CvType.CV_8UC1, new Scalar(0));

        Mat     hsv_image           = new Mat(size, CvType.CV_8UC1);
        Mat     lower_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat     upper_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat     red_hue_image       = new Mat(size, CvType.CV_8UC1);
        Point   center              = new Point();
        float[] radii               = new float[1];
        int     radius;

        Mat closing    = new Mat(size, CvType.CV_8UC1);
        Mat kernel_9x9 = new Mat(new Size(11, 11), CvType.CV_8UC1, new Scalar(1));

        Imgproc.cvtColor(crop_img, hsv_image, Imgproc.COLOR_BGR2HSV);
        Core.inRange(hsv_image, new Scalar(0, 70, 50), new Scalar(10, 255, 255), lower_red_hue_range);
        Core.inRange(hsv_image, new Scalar(150, 70, 50), new Scalar(179, 255, 255), upper_red_hue_range);

        Core.addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);

        Mat hierarchy = new Mat();


        // TODO: 18-May-18 remove
        Bitmap tempdisp6 = Bitmap.createBitmap(red_hue_image.cols(), red_hue_image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(red_hue_image, tempdisp6);


        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        Imgproc.morphologyEx(red_hue_image, closing, Imgproc.MORPH_ERODE, kernel_9x9);

        Imgproc.findContours(red_hue_image, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        int    maxAreaIdx = 0;
        double maxArea    = Imgproc.contourArea(contours.get(0));
        for (int i = 1; i < contours.size(); i++) {
            if (Imgproc.contourArea(contours.get(i)) > maxArea) {
                maxArea = Imgproc.contourArea(contours.get(i));
                maxAreaIdx = i;
            }
        }
        MatOfPoint2f contour0 = new MatOfPoint2f(contours.get(maxAreaIdx).toArray());
        Imgproc.minEnclosingCircle(contour0, center, radii);
        Imgproc.drawContours(crop_img, contours, maxAreaIdx, new Scalar(0, 0, 255), 1);
        Bitmap tempdisp7 = Bitmap.createBitmap(crop_img.cols(), crop_img.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(crop_img, tempdisp7);
        radius = (int) (radii[0]) - 7;
        Imgproc.circle(mask, center, radius, new Scalar(255, 255, 255), -1);
        Mat fg = new Mat();
        Core.bitwise_or(crop_img, crop_img, fg, mask);
        Imgproc.cvtColor(fg, fg, Imgproc.COLOR_BGR2RGB);
        Bitmap tempdisp5 = Bitmap.createBitmap(fg.cols(), fg.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(fg, tempdisp5);


        Scalar mean_val = Core.mean(lab_image, mask);
        Double Lblood   = mean_val.val[0];
        Lblood = Math.round(Lblood * 1e4) / 1e4;
        return Lblood;

    }

    private static Double[] getLValues(Mat image) {
        Size   size = image.size();
        double M    = size.height;
        double N    = size.width;
        //Mat crop_img = image.submat(75, (int) (M), 50, 50 + (int) (N - 500));

        Mat gray_image = new Mat(size, CvType.CV_8UC1);
        Mat smooth     = new Mat(size, CvType.CV_8UC1);
        Mat lab_image  = new Mat(size, CvType.CV_8UC1);
        Mat hsv_image  = new Mat(size, CvType.CV_8UC1);
        Mat thresh     = new Mat(size, CvType.CV_8UC1);
        Mat closing    = new Mat(size, CvType.CV_8UC1);

        Mat kernel_9x9 = new Mat(new Size(11, 11), CvType.CV_8UC1, new Scalar(1));

        Mat lower_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat upper_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat red_hue_image       = new Mat(size, CvType.CV_8UC1);

        Imgproc.cvtColor(image, gray_image, Imgproc.COLOR_BGR2GRAY);
        //Imgproc.blur(image, smooth, new Size(5, 5));
        Imgproc.cvtColor(image, lab_image, Imgproc.COLOR_BGR2Lab);
        Imgproc.cvtColor(image, hsv_image, Imgproc.COLOR_BGR2HSV);

        // TODO: 18-Aug-18
        //        Core.inRange(hsv_image, new Scalar(39, 0, 0), new Scalar(100, 255, 255), lower_red_hue_range);
        //        Core.inRange(hsv_image, new Scalar(25, 50, 0), new Scalar(39, 255, 255), upper_red_hue_range);
        Core.inRange(hsv_image, new Scalar(0, 70, 50), new Scalar(10, 255, 255), lower_red_hue_range);
        Core.inRange(hsv_image, new Scalar(150, 70, 50), new Scalar(179, 255, 255), upper_red_hue_range);

        Core.addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);

        Bitmap tempdisp1 = Bitmap.createBitmap(red_hue_image.cols(), red_hue_image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(red_hue_image, tempdisp1);

//        Imgproc.threshold(gray_image, thresh, 127, 255, Imgproc.THRESH_BINARY_INV);
//
//        Bitmap tempdisp1 = Bitmap.createBitmap(thresh.cols(), thresh.rows(), Bitmap.Config.ARGB_8888);
//        Utils.matToBitmap(thresh, tempdisp1);

        Imgproc.morphologyEx(red_hue_image, closing, Imgproc.MORPH_CLOSE, kernel_9x9);

        tempdisp1 = Bitmap.createBitmap(closing.cols(), closing.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(closing, tempdisp1);

        Mat hierarchy = new Mat();

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(closing, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        int        maxAreaIdx = 0;
        double     maxArea    = Imgproc.contourArea(contours.get(maxAreaIdx));
        MatOfPoint maxi       = contours.get(maxAreaIdx);
        MatOfPoint temp_contour;

        Rect xywh = boundingRect(contours.get(0));
        int  diff = (int) (xywh.height / 6);
        int  x1   = xywh.x + 30;
        int  y1   = xywh.y + 20;
        int  x2   = xywh.x + xywh.width - 40;
        int  y2   = y1 + (diff - 35);

        Double[] Lshades = new Double[6];
//

        for (int i = 0; i < 6; i++) {
            Mat subRects = lab_image.submat(y1, y2, x1, x2);

            tempdisp1 = Bitmap.createBitmap(subRects.cols(), subRects.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(subRects, tempdisp1);

            Scalar avgLab = Core.mean(subRects);
            Lshades[i] = avgLab.val[0];
            Imgproc.rectangle(lab_image, new Point(x1, y1), new Point(x2, y2), new Scalar(0, 255, 0), 2);
            y1 = y2 + 35;
            y2 = y1 + (diff - 35);
        }

        Bitmap tempdisp2 = Bitmap.createBitmap(lab_image.cols(), lab_image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(lab_image, tempdisp2);

        return Lshades;
    }

}





