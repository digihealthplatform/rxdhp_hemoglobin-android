package com.dhp.hemoglobin.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.hemoglobin.LApplication;
import com.dhp.hemoglobin.R;
import com.dhp.hemoglobin.opencv.ImageProcessing;
import com.dhp.hemoglobin.utils.LPref;

import static com.dhp.hemoglobin.utils.LConstants.DEFAULT_IP_ADDRESS;
import static com.dhp.hemoglobin.utils.LConstants.FOLDER_PATH;
import static com.dhp.hemoglobin.utils.LConstants.TYPE_TEST;
import static com.dhp.hemoglobin.utils.LPref.KEY_IP_ADDRESS;



public class ActivityCameraView extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private int currentContrast = 0;
    private int currentIso      = 0;

    private int activityType;
    File imgFile1,imgFile2,imgFile3,imgFile;
    private final int CONTRAST_MIN = 0;
    private final int CONTRAST_MAX = 6;
    private final int ISO_MIN      = 0;
    private       int ISO_MAX      = 6;

    private String[] isoValues;

    private static final String TAG = "ActivityCameraView";

    public static final String HB_FOLDER_PATH = ".Hemoglobin";

    private Mat rgba;
    private Bitmap[] bitmap = new Bitmap[8];

    private ProgressDialog progressDialog;

    private AlertDialog waitDialog;
    private View        dialogView;

    @BindView(R.id.jc_view)
    JavaCameraView jcView;

    @BindView(R.id.btn_iso_plus)
    Button btnIsoPlus;

    @BindView(R.id.btn_iso_minus)
    Button btnIsoMinus;

    @BindView(R.id.tv_iso_value)
    TextView tvIsoValue;

    @BindView(R.id.btn_contrast_plus)
    Button btnContrastPlus;

    @BindView(R.id.btn_contrast_minus)
    Button btnContrastMinus;

    @BindView(R.id.tv_contrast_value)
    TextView tvContrastValue;

    @BindView(R.id.btn_calibrate)
    Button btnCalibrate;

    @BindView(R.id.btn_capture)
    Button btnCapture;

    @BindView(R.id.ll_contrast)
    LinearLayout llContrast;

    @BindView(R.id.ll_saturation)
    LinearLayout llSaturation;

    private HttpURLConnection connection;
    private Context           context;

    String serverResponseMessage;
    int    serverResponseCode;

    long requestLength;

    private BaseLoaderCallback loaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    jcView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera_view);
        ButterKnife.bind(this);

        activityType = getIntent().getIntExtra("activity_type", 0);

        if (TYPE_TEST == activityType) {
            btnCalibrate.setVisibility(View.GONE);
            llContrast.setVisibility(View.GONE);
            llSaturation.setVisibility(View.GONE);

        } else {
            btnCapture.setVisibility(View.GONE);
        }

        jcView.setVisibility(SurfaceView.VISIBLE);
        jcView.setCvCameraViewListener(this);

        jcView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            }
        });

        btnContrastPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnContrastMinus.setEnabled(true);

                ++currentContrast;

                if (CONTRAST_MAX <= currentContrast) {
                    currentContrast = CONTRAST_MAX;
                    btnContrastPlus.setEnabled(false);
                } else {
                    btnContrastPlus.setEnabled(true);
                }

                jcView.setContrast(currentContrast);

                LPref.putIntPref("contrast", currentContrast);
                tvContrastValue.setText("" + currentContrast);
            }
        });

        btnContrastMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnContrastPlus.setEnabled(true);

                --currentContrast;

                if (CONTRAST_MIN >= currentContrast) {
                    btnContrastMinus.setEnabled(false);
                    currentContrast = CONTRAST_MIN;
                } else {
                    btnContrastMinus.setEnabled(true);
                }

                jcView.setContrast(currentContrast);

                LPref.putIntPref("contrast", currentContrast);
                tvContrastValue.setText("" + currentContrast);
            }
        });

        btnIsoPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnIsoMinus.setEnabled(true);

                ++currentIso;

                if (ISO_MAX <= currentIso) {
                    currentIso = ISO_MAX;
                    btnIsoPlus.setEnabled(false);
                } else {
                    btnIsoPlus.setEnabled(true);
                }

                jcView.setIso(isoValues[currentIso]);
                LPref.putIntPref("iso", currentIso);

                tvIsoValue.setText(isoValues[currentIso]);
            }
        });

        btnIsoMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnIsoPlus.setEnabled(true);

                --currentIso;

                if (ISO_MIN >= currentIso) {
                    currentIso = ISO_MIN;
                    btnIsoMinus.setEnabled(false);
                } else {
                    btnIsoMinus.setEnabled(true);
                }

                jcView.setIso(isoValues[currentIso]);

                LPref.putIntPref("iso", currentIso);
                tvIsoValue.setText(isoValues[currentIso]);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, loaderCallback);
        } else {
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        rgba = new Mat(height, width, CvType.CV_8UC4);

        currentContrast = LPref.getIntPref("contrast", -1);

        if (-1 == currentContrast) {
            currentContrast = jcView.getContrast();
        }

        jcView.setContrast(currentContrast);

        isoValues = jcView.getIsoValues().split(",");

        ISO_MAX = isoValues.length;

        btnIsoMinus.setEnabled(false);

        tvContrastValue.setText("" + currentContrast);

        currentIso = LPref.getIntPref("iso", 0);

        jcView.setIso(isoValues[currentIso]);
        tvIsoValue.setText("" + isoValues[currentIso]);

        if (CONTRAST_MIN >= currentContrast) {
            btnContrastMinus.setEnabled(false);
            currentContrast = CONTRAST_MIN;
        } else {
            btnContrastMinus.setEnabled(true);
        }

        if (ISO_MIN >= currentIso) {
            currentIso = ISO_MIN;
            btnIsoMinus.setEnabled(false);
        } else {
            btnIsoMinus.setEnabled(true);
        }
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        rgba = inputFrame.rgba();


        if (TYPE_TEST == activityType) {
            //capture
            Point tl = new Point(rgba.width() * 0.46, rgba.height() * 0.45);
            Point br = new Point(rgba.width() * 0.54, rgba.height() * 0.6);
            Imgproc.rectangle(rgba, tl, br, new Scalar(0, 255, 0), 5);
            return rgba;
        } else {
            Point tl = new Point(rgba.width() * 0.15, rgba.height() * 0.1);
            Point br = new Point(rgba.width() * 0.4, rgba.height() * 0.9);
            Imgproc.rectangle(rgba, tl, br, new Scalar(0, 255, 0), 5);
            return rgba;
        }
    }

    @Override
    public void onCameraViewStopped() {

    }

    int pendingTime = 5;
    int a=0;

    private void count() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                --pendingTime;

                if (0 == pendingTime) {
                    if(TYPE_TEST==activityType)
                    {
                        capture();
                        a++;
                        if(a<3){
                            pendingTime=5;
                            count();
                        }
                    }
                    else capture();
                } else {
                    ((TextView) dialogView.findViewById(R.id.tv_wait)).setText("" + pendingTime);
                    count();
                }
            }
        }, 1000);
    }

    @OnClick({R.id.btn_calibrate, R.id.btn_capture})
    void onCapture() {
        a=0;
        dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_wait, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setTitle("Wait...");
        builder.setView(dialogView);

        waitDialog = builder.create();
        waitDialog.setCanceledOnTouchOutside(false);
        waitDialog.show();
        ((TextView) dialogView.findViewById(R.id.tv_wait)).setText("" + pendingTime);

        count();
    }

    void capture() {
        // Dismiss loader
        if(a==2){
            waitDialog.dismiss();
        }


        Mat paramRegion;

        if (TYPE_TEST == activityType) {
            //capture
            Point tl   = new Point(rgba.width() * 0.46, rgba.height() * 0.45);
            Point br   = new Point(rgba.width() * 0.54, rgba.height() * 0.6);
            Rect  crop = new Rect(tl, br);

            paramRegion = rgba.submat(crop);
        } else {
            Point tl   = new Point(rgba.width() * 0.15, rgba.height() * 0.1);
            Point br   = new Point(rgba.width() * 0.4, rgba.height() * 0.9);
            Rect  crop = new Rect(tl, br);

            paramRegion = rgba.submat(crop);
        }


        bitmap[0] = Bitmap.createBitmap(paramRegion.cols(), paramRegion.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(paramRegion, bitmap[0]);



        if (TYPE_TEST == activityType) {
            switch (a)
            {
                case 0:
                    imgFile1= saveTempImage(bitmap[0], "capture1_");
                    break;
                case 1:
                    imgFile2 = saveTempImage(bitmap[0], "capture2_");
                    break;
                case 2:
                    imgFile3 = saveTempImage(bitmap[0], "capture3_");
                    break;
            }

            if(a==2){
                //a=0;
                processFile(imgFile1,imgFile2,imgFile3);
            }
        } else {
            imgFile = saveTempImage(bitmap[0], "calibration_");
            processFile(imgFile,null,null);
        }

    }

    private void processFile(final File imgFile1,final File imgFile2, final File imgFile3) {
        showLoader();

        new Thread(new Runnable() {
            @Override
            public void run() {
//                uploadFile(imgFile.getAbsolutePath());

                if (TYPE_TEST == activityType) {
                    float Hb = 0;
                   try {
                        Hb = ImageProcessing.findHb(imgFile1.getPath(),imgFile2.getPath(),imgFile3.getPath(),
                                Environment.getExternalStorageDirectory() + "/" + HB_FOLDER_PATH + "/" + LPref.getStringPref("calibration_file", ""));

                        LApplication.saveResult("" + Hb);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                finishTask(imgFile1.getName());
                            }
                        });

                   }
            catch (Exception e) {
                e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pendingTime = 5;
                                progressDialog.dismiss();
                                Toast.makeText(ActivityCameraView.this, "Unable to process. Try again", Toast.LENGTH_SHORT).show();
                            }
                     });

                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            finishTask(imgFile.getName());
                        }
                    });
                }
            }
        }).start();
    }

    private void showLoader() {
        progressDialog = new ProgressDialog(this, R.style.AlertDialogLight);

        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    private void finishTask(String fileName) {
        progressDialog.dismiss();

        if (TYPE_TEST == activityType) {
            startActivity(new Intent(ActivityCameraView.this, ActivityTestResult.class));

        } else {
            LPref.putStringPref("calibration_file", fileName);
            Toast.makeText(ActivityCameraView.this, "Calibrated", Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    private File saveTempImage(Bitmap bitmapImage, String name) {
        File folder = new File(Environment.getExternalStorageDirectory(), HB_FOLDER_PATH);

        SimpleDateFormat sdf  = new SimpleDateFormat("dd_MM_yyyy-HH_mm_ss");
        Date             date = new Date();

        String fileName = name + sdf.format(date) + "-iso_" + isoValues[currentIso] + "-C_" + currentContrast + ".png";
        File   imgFile  = new File(folder, fileName);

        try {
            FileOutputStream fos = new FileOutputStream(imgFile);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return imgFile;
    }

    public void onDestroy() {
        super.onDestroy();

        if (jcView != null)
            jcView.disableView();
    }
}