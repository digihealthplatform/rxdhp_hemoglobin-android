package com.dhp.hemoglobin.ui;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.hemoglobin.BaseActivity;
import com.dhp.hemoglobin.BaseActivityTest;
import com.dhp.hemoglobin.R;
import com.dhp.hemoglobin.utils.LConstants;

import static com.dhp.hemoglobin.utils.LConstants.TYPE_CALIBRATE;

public class ActivityMain extends BaseActivity {

    public static final int REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initToolbar("Hemoglobin", false);

        createFoldersWithPermission();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_settings:
                startActivity(new Intent(this, ActivitySettings.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_calibrate)
    void calibrate() {
        Intent intent = new Intent(this, ActivityCameraView.class);
        intent.putExtra("activity_type", TYPE_CALIBRATE);
        startActivity(intent);
    }

    @OnClick(R.id.btn_start_test)
    void startTest() {
        Intent intent = new Intent(this, ActivityTestStep1.class);
        startActivity(intent);
    }

    private void createFoldersWithPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE);

        } else {
            createFolders();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createFolders();
                } else {
                    Toast.makeText(this, "Please allow the permissions", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
            break;
        }
    }

    private void createFolders() {
        File folder = new File(Environment.getExternalStorageDirectory(), LConstants.FOLDER_PATH);

        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    @Override
    public void onBackPressed() {
        confirmBackPressed();
    }

    private void confirmBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setMessage(getString(R.string.message_confirm_exit));
        builder.setPositiveButton(getString(R.string.btn_exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityMain.super.onBackPressed();
            }
        });

        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }
}