package com.dhp.hemoglobin.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.hemoglobin.BaseActivity;
import com.dhp.hemoglobin.R;
import com.dhp.hemoglobin.utils.LPref;

import static com.dhp.hemoglobin.utils.LConstants.DEFAULT_DURATION;
import static com.dhp.hemoglobin.utils.LConstants.DEFAULT_IP_ADDRESS;
import static com.dhp.hemoglobin.utils.LPref.KEY_DURATION;
import static com.dhp.hemoglobin.utils.LPref.KEY_IP_ADDRESS;

public class ActivitySettings extends BaseActivity {

//    @BindView(R.id.et_ip_address)
//    EditText etAddress;

    @BindView(R.id.et_duration)
    EditText etDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        initToolbar("Settings", true);

//        etAddress.setText(LPref.getStringPref(KEY_IP_ADDRESS, DEFAULT_IP_ADDRESS));
        etDuration.setText("" + LPref.getIntPref(LPref.KEY_DURATION, DEFAULT_DURATION));
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        try {
            LPref.putIntPref(KEY_DURATION, Integer.parseInt(etDuration.getText().toString()));
        } catch (Exception e) {

        }
//        LPref.putStringPref(KEY_IP_ADDRESS, etAddress.getText().toString());

        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
        finish();
    }
}