package com.dhp.hemoglobin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.dhp.hemoglobin.BaseActivity;
import com.dhp.hemoglobin.BaseActivityTest;
import com.dhp.hemoglobin.LApplication;
import com.dhp.hemoglobin.R;

public class ActivityTestResult extends BaseActivity {

    @BindView(R.id.tv_hemoglobin)
    TextView tvHemoglobin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_result);
        ButterKnife.bind(this);
        initToolbar("Result", true);
        String result = LApplication.getResult();

        try {
            double f = Double.parseDouble(result);
            tvHemoglobin.setText("Hemoglobin: " + String.format("%.2f", new BigDecimal(f)) + " g/dl");
        } catch (Exception e) {
            tvHemoglobin.setText("Hemoglobin: " + "___" + " g/dl");
        }
    }

    @OnClick(R.id.btn_start_test)
    void startNewTest() {
        startActivity(new Intent(this, ActivityTestStep1.class));
        finish();
    }
}