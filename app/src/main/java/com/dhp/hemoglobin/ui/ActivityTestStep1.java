package com.dhp.hemoglobin.ui;

import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.hemoglobin.BaseActivityTest;
import com.dhp.hemoglobin.R;

public class ActivityTestStep1 extends BaseActivityTest {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_step_1);
        ButterKnife.bind(this);
        initToolbar("Step 1", true);
    }

    @OnClick(R.id.btn_next)
    void onNext() {
        Intent intent = new Intent(this, ActivityTestStep2.class);
        startActivity(intent);

        finish();
    }
}