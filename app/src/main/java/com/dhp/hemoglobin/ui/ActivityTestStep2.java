package com.dhp.hemoglobin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.hemoglobin.BaseActivityTest;
import com.dhp.hemoglobin.R;
import com.dhp.hemoglobin.utils.LPref;

import static com.dhp.hemoglobin.utils.LConstants.DEFAULT_DURATION;
import static com.dhp.hemoglobin.utils.LConstants.TYPE_TEST;

public class ActivityTestStep2 extends BaseActivityTest {

    private int pendingTime = LPref.getIntPref(LPref.KEY_DURATION, DEFAULT_DURATION);

    @BindView(R.id.btn_next)
    Button btnNext;

    @BindView(R.id.tv_duration)
    TextView tvDuration;

    @BindView(R.id.tv_description)
    TextView tvDescription;

    @BindView(R.id.tv_wait)
    TextView tvWait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_step_2);
        ButterKnife.bind(this);
        initToolbar("Step 2", true);
    }

    @OnClick(R.id.btn_next)
    void onNext() {
        btnNext.setVisibility(View.GONE);
        tvDescription.setVisibility(View.GONE);
        tvDuration.setText("" + pendingTime);

        count();
    }

    private void count() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                --pendingTime;
                tvDuration.setText("" + pendingTime);

                if (0 == pendingTime) {
                    showNext();
                } else {
                    count();
                }
            }
        }, 1000);
    }

    private void showNext() {
        Intent intent = new Intent(this, ActivityCameraView.class);
        intent.putExtra("activity_type", TYPE_TEST);
        startActivity(intent);

        finish();
    }
}