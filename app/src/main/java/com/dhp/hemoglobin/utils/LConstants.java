package com.dhp.hemoglobin.utils;

public class LConstants {
    public static final int    DEFAULT_DURATION   = 30;
    public static final String DEFAULT_IP_ADDRESS = "192.168.0.4";

    public static final int TYPE_CALIBRATE = 110;
    public static final int TYPE_TEST      = 111;

    public static final String FOLDER_PATH = ".Hemoglobin";
}